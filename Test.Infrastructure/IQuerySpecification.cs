﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Infrastructure
{
    public interface IQuerySpecification<T>
        where T : class
    {
        IQueryable<T> Specify(IQueryable<T> query);
    }
}
