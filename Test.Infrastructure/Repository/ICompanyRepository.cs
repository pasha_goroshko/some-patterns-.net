﻿using Test.Domain;

namespace Test.Infrastructure.Repository
{
    public interface ICompanyRepository : IRepository<Company>
    {
    }
}
