﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Domain;

namespace Test.Infrastructure.Repository
{
    public interface IEmployeeRepository:IRepository<Employee>
    {
    }
}
