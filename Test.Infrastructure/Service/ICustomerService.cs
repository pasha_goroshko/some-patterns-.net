﻿using System.Collections.Generic;
using Test.Infrastructure.Service.Models;

namespace Test.Infrastructure.Service
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetAll();
        void DeleteCustomer(int idCustomer);
        Customer AddCustomer(Customer customer);
    }
}
