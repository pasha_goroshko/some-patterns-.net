﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Infrastructure.Service.Models;

namespace Test.Infrastructure.Service
{
    public interface IDepartmentService
    {
        IEnumerable<Department> GetAll();
        void Delete(int idDepartment);
        Department AddDepartment(Department department);
    }
}
