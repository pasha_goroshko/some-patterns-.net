﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Infrastructure.Service.Models;

namespace Test.Infrastructure.Service
{
    public interface ICompanyService
    {
        IEnumerable<Company> GetAll();
        void AddCompany(Company company);
    }
}
