﻿using EmitMapper;
using System.Collections.Generic;
using System.Linq;
using Test.Infrastructure.Repository;
using Test.Infrastructure.Service;
using Test.Infrastructure.Service.Models;

namespace Test.Service
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository companyRepository;

        public CompanyService(ICompanyRepository companyRepository)
        {
            this.companyRepository = companyRepository;
        }
        public void AddCompany(Company company)
        {
            companyRepository.Add(ObjectMapperManager.DefaultInstance.GetMapper<Company, Test.Domain.Company>().Map(company));
        }
        public IEnumerable<Company> GetAll()
        {
            return companyRepository.GetAll().Select(CompanyMap);
        }

        private Company CompanyMap(Test.Domain.Company company)
        {
            return ObjectMapperManager.DefaultInstance.GetMapper<Test.Domain.Company, Company>().Map(company);
        }
    }
}
