﻿using System;
using System.Collections.Generic;
using Test.Infrastructure.Repository;
using Test.Infrastructure.Service;
using Test.Infrastructure.Service.Models;

namespace Test.Service
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository employeeRepository;
        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }
        public void AddEmployee(Employee employee, int idDepartment)
        {
            throw new NotImplementedException();
        }

        public void DeleteEmployee(int idEmployee)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Employee> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
