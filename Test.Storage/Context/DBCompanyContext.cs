﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Domain;

namespace Test.Storage.Context
{
    public class DBCompanyContext : DbContext
    {
        public DBCompanyContext()
            : base("DBCompanyContext")
        {
            this.Database.Log = s => Debug.Write(s);
        }

        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Department> Departmens { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
    }
}
