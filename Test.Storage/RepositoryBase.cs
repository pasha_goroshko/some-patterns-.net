﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Infrastructure;

namespace Test.Storage
{
    public abstract class RepositoryBase<T> where T : class
    {
        private DbContext dataContext;
        private readonly IDbSet<T> dbSet;
        private readonly IList<IQuerySpecification<T>> specificators;

        protected RepositoryBase(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            dataContext = databaseFactory.Get();
            dbSet = dataContext.Set<T>();
            specificators = new List<IQuerySpecification<T>>();
        }
        protected IDatabaseFactory DatabaseFactory { get; private set; }
        protected virtual IDbSet<T> DbSet { get { return dbSet; } }

        protected DbContext DataContext { get { return dataContext; } }

        public virtual IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }

        public virtual void Add(T entity)
        {
            DbSet.Add(entity);
        }

        public virtual void Delete(T entity)
        {
            DbSet.Remove(entity);
        }
    }
}
