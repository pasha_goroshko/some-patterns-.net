﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Storage.Context;

namespace Test.Storage
{
    public class DatabaseFactory : IDatabaseFactory, IDisposable
    {
        private Lazy<DbContext> dataContext = new Lazy<DbContext>(() => new DBCompanyContext());
        private bool disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public DbContext Get()
        {
            return dataContext.Value;
        }

        public void Initialize()
        {
            Get().Database.Initialize(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if(disposing)
            {
                if(dataContext.IsValueCreated)
                {
                    dataContext.Value.Dispose();
                }
            }
            disposed = true;
        }
        ~DatabaseFactory()
        {
            Dispose(false);
        }
    }
}
