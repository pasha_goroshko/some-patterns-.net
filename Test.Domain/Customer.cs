﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Domain
{
    public class Customer : People
    {
        public string AdditionalInformation { get; set; }
    }
}
