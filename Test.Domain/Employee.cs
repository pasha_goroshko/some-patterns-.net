﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Domain
{
    /// <summary>
    /// Сотрудники.
    /// </summary>
    public class Employee : People
    {
        /// <summary>
        /// Стаж работы.
        /// </summary>
        public int Experience { get; set; }
    }
}
