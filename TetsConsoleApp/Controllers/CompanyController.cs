﻿using EmitMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Infrastructure.Service;
using Test.Infrastructure.Service.Models;
using TetsConsoleApp.Models;

namespace TetsConsoleApp.Controllers
{
    class CompanyController
    {
        private readonly ICompanyService companyService;

        public CompanyController(ICompanyService companyService)
        {
            this.companyService = companyService;
        }

        public IEnumerable<CompanyModel> GetAll()
        {
            return companyService.GetAll().Select(CompanyToCompanyModel);
        }

        public void AddCompany(CompanyModel company)
        {
            companyService.AddCompany(ObjectMapperManager.DefaultInstance.GetMapper<CompanyModel, Company>().Map(company));
        }

        private CompanyModel CompanyToCompanyModel(Company company)
        {
            return ObjectMapperManager.DefaultInstance.GetMapper<Company, CompanyModel>().Map(company);
        }
    }
}
