﻿using Autofac;
using EmitMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Test.Infrastructure.Service;
using Test.Service;
using Test.Storage.Repository;
using TetsConsoleApp.Controllers;
using TetsConsoleApp.Models;

namespace TetsConsoleApp
{
    class Program
    {
        static ContainerBuilder builder;
        static void Main(string[] args)
        {
            var test = "test value";
            builder = new ContainerBuilder();

            builder.RegisterType<CompanyRepository>()
                .As<ICompanyService>()
                .InstancePerLifetimeScope();

           

            var container = builder.Build();
            //container.Resolve<CompanyController>();

            using (var scope = container.BeginLifetimeScope())
            {
                var r = scope.Resolve<ICompanyService>();
                CompanyController cntr = new CompanyController(r);
                Console.WriteLine("example = " + cntr.GetAll().Count());
            }
            Console.ReadLine();
            //CompanyController controller;
            //var e = controller.GetAll();

        }
    }
}