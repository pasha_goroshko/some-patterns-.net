﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TetsConsoleApp.Models
{
    /// <summary>
    /// Отделение.
    /// </summary>
    public class DepartmentModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string About { get; set; }
        public IEnumerable<EmployeeModel> Employees { get; set; }
    }
}
