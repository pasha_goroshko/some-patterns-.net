﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TetsConsoleApp.Models
{
    public class CompanyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string About { get; set; }
        public IEnumerable<DepartmentModel> Departmens { get; set; }
        public IEnumerable<CustomerModel> Customers { get; set; }
    }
}
