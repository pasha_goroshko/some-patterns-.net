﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TetsConsoleApp.Models
{
    /// <summary>
    /// Сотрудники.
    /// </summary>
    public class EmployeeModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
        /// <summary>
        /// Стаж работы.
        /// </summary>
        public int Experience { get; set; }
    }
}
